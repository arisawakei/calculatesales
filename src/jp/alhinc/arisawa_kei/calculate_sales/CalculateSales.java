package jp.alhinc.arisawa_kei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {
	public static void main(String[] args) {
		if(args.length != 1) { //コマンドライン引数が1つ以外の場合のエラー処理
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		BufferedReader br = null;
		HashMap<String, Long> sales = new HashMap<String, Long>();
		HashMap<String, String> branchNames = new HashMap<String, String>();
		ArrayList<String> salesFile = new ArrayList<String>();
		String fileName = "branch.lst";
		String fileOutName = "branch.out";
		String storeDefine = "支店定義";
		String codeRule = "\\d{3}";

		if(!fileRead(args[0], branchNames, sales, fileName, storeDefine, codeRule)) {
			return;
		}

		// 数字8桁.rcd且つファイルであるファイル名をArralyListに格納
		File dir1 = new File(args[0]);
		String[] salesFileList = dir1.list();
		for(String names : salesFileList) {
			File toFile = new File(args[0], names);
			if(names.matches("\\d{8}.rcd") && toFile.isFile()){
				salesFile.add(names);
			}
		}
		//売上ファイル名が連番になっていない場合のエラー処理
		for(int i = 0; i < salesFile.size()-1; i++) {
			String name = salesFile.get(i).substring(0, 8);
			String namePlus = salesFile.get(i + 1).substring(0, 8);
			int intName = Integer.parseInt(name);
			int intPlus = Integer.parseInt(namePlus);
			if(intPlus - intName != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		// 数字8桁.rcdで終わるファイルの中身をArrayListに格納しsalesのvalueに代入
		try {
			for(int j = 0; j < salesFile.size(); j++) {
				ArrayList<String> codeSales = new ArrayList<String>();
				File file = new File(args[0], salesFile.get(j));
				br = new BufferedReader(new FileReader(file));
				String readSales;
				while((readSales = br.readLine()) !=null) {
					codeSales.add(readSales);
				}
				//売上ファイルの行数が2行ではなかった場合のエラー処理
				if(codeSales.size() != 2) {
					System.out.println(salesFile.get(j) + "のフォーマットが不正です");
					return;
				}
				//該当支店がない場合のエラー処理」
				if(!sales.containsKey(codeSales.get(0))) {
					System.out.println(salesFile.get(j) + "の支店コードが不正です");
					return;
				}
				//売上額が数字以外の場合のエラー処理
				if(!codeSales.get(1).matches("^[0-9０-９]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//ここから加算
				long castLong = Long.parseLong(codeSales.get(1));
				long calculate = sales.get(codeSales.get(0)) + castLong;
				long digit = 9999999999L;
				//合計金額が10桁を超えた場合のエラー処理

				if(calculate > digit) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
					//支店コードと合計額をsalesに代入
					sales.put(codeSales.get(0), calculate);
				}
		}catch (IOException e) { //例外キャッチのエラー処理
			System.out.println("予期せぬエラーが発生しました");
			return;
		}catch (NumberFormatException e) { //例外キャッチのエラー処理
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) { //例外キャッチのエラー処理
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		if(!fileWrite(args[0], branchNames, sales, fileOutName)) {
			return;
		}
	}

	//ファイルを読み込み2つのマップ(支店コード, 支店名), (支店コード, 0)をつくるメソッド
	public static boolean fileRead
	(String filePath, HashMap<String, String> branchNames, HashMap<String, Long> sales, String fileName, String storeDefine, String codeRule) {
		BufferedReader br = null;

		try {
			File file = new File(filePath, fileName);
			//支店定義ファイルが存在しない場合のエラー処理
			if(!file.exists()) {
				System.out.println(storeDefine + "ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String branchList;
			while((branchList = br.readLine()) != null) {
				String[] branches = branchList.split(",");
				//ここからエラー処理
				//数字3桁ではない場合のエラー処理
				if (!branches[0].matches(codeRule)){
					System.out.println(storeDefine + "ファイルのフォーマットが不正です");
					return false;
				}
				//要素数が2個以外の場合のエラー処理
				if (branches.length != 2) {
					System.out.println(storeDefine + "ファイルのフォーマットが不正です");
					return false;
				}
				//ここまでエラー処理
				branchNames.put(branches[0], branches[1]); //支店コードと支店名をマップに格納
				sales.put(branches[0], (long) 0); //支店コードと合計金額(この時点では0)をマップに格納
			}
		} catch (IOException e) { //例外キャッチのエラー処理
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) { //closeできなかった場合のエラー処理
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//加算結果をbranch.outに出力
	public static boolean fileWrite
	(String filePath, HashMap<String, String> branchNames, HashMap<String, Long> sales, String fileName) {
		BufferedWriter bw = null;

		try {
			File file =new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) { //close処理ができなかった場合のエラー処理
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) { //例外キャッチのエラー処理
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}




